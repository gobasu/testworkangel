<?php
/**
 * Base config for App Module
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController'
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
                'list-users' => array(
                    'options' => array(
                        'route'    => 'merchant <id>',
                        'defaults' => array(
                            'controller' => 'Application\Controller\Index',
                            'action'     => 'Test'
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Application\Model\CurrencyConverter' => function ($sm) {
                $model = new \Application\Model\CurrencyConverter();
                $model->setAdapter($sm->get('db'));
                $model->setCurrencyWebService($sm->get('Application\Model\CurrencyWebservice'));
                return $model;
            },
            'Application\Model\Merchant' => function ($sm) {
                $model = new \Application\Model\Merchant();
                $model->setAdapter($sm->get('db'));
                $model->setTransactionsModel($sm->get('Application\Model\Transactions'));
                return $model;
            },
            'Application\Model\Transactions' => function ($sm) {
                $model = new \Application\Model\Transactions();
                $model->setAdapter($sm->get('db'));
                return $model;
            },
        ),
        'invokables' => array(
            'Application\Model\CurrencyWebservice' => 'Application\Model\CurrencyWebservice',
        ),
    ),
);
