<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Console\Request as ConsoleRequest;

/**
 * Main app class - provides main app action
 * Class IndexController
 * @package Application\Controller
 */
class IndexController extends AbstractActionController
{
    public function testAction()
    {
        $request = $this->getRequest();
        // Make sure that we are running in a console and the user has not tricked our
        // application into running this action from a public web server.
        if (!$request instanceof ConsoleRequest) {
            throw new \RuntimeException('You can only use this action from a console!');
        }
        try {
            $id_merchant = $this->params()->fromRoute('id');
            /**
             * @var $merchantModel \Application\Model\Merchant
             */
            $merchantModel = $this->serviceLocator->get('Application\Model\Merchant');
            $merchant = $merchantModel->getMerchant($id_merchant);
            $transactions = $merchant->getTransactions();

            /**
             * @var $currencyConverter \Application\Model\CurrencyConverter
             */
            $currencyConverter = $this->serviceLocator->get('Application\Model\CurrencyConverter');
            $transactions = $currencyConverter->convertAndSum($transactions);

            $response = "Merchant income at: \n";
            foreach ($transactions['sum'] as $date => $sum) {
                $response .= $date . " " . $sum . " Pounds \n";
            }
            return $response;
        } catch (\InvalidArgumentException $e) {
            print_r($e->getMessage() . "\n");
        }
    }
}
