<?php

/**
 * Dummy web service returning random exchange rates
 *
 */
namespace Application\Model;

class CurrencyWebservice
{

    /**
     * Return random value here for basic currencies like GBP USD EUR (simulates real API)
     * @param $currency
     * @return float
     */
    public function getExchangeRate($currency)
    {
        $tmp = array();
        if (empty($tmp[$currency])) {
            $tmp[$currency] = mt_rand() / mt_getrandmax();
        }
        return $tmp[$currency];
    }
}