<?php

/**
 * Created by PhpStorm.
 * User: Michał
 * Date: 2015-01-10
 * Time: 22:07
 */
namespace Application\Model;

use Zend\Db\ResultSet\ResultSet;

class Transactions extends AbstractModel
{

    const ERR_MERCHANT_ID_NUMERIC = 'Merchant Id must be numeric';
    const ERR_TRANSACTION_LIST_IS_EMPTY = 'Transactions list for current Merchant Id is EMPTY';

    /**
     * Provides merchant transactions list
     * @param int $id_merchant
     * @param int $limit
     * @return ResultSet
     */
    public function getTransaction($id_merchant, $limit = 10)
    {
        //Check if merchant id is numeric - otwherwise throw exception
        if (!is_numeric($id_merchant)) {
            throw new \InvalidArgumentException(self::ERR_MERCHANT_ID_NUMERIC);
        }
        $pdoRes = $this->adapter
            ->query('SELECT date, value FROM income WHERE id_merchant=' . $id_merchant . ' LIMIT ' . $limit)->execute();
        if(!$pdoRes->count()) {
            throw new \InvalidArgumentException(self::ERR_TRANSACTION_LIST_IS_EMPTY);
        }
        $resultSet = new ResultSet();
        $resultSet->initialize($pdoRes);
        return $resultSet;
    }
}