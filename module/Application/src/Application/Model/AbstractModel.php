<?php
/**
 * Created by PhpStorm.
 * User: Michał
 * Date: 2015-01-10
 * Time: 22:07
 */
namespace Application\Model;

use \Zend\Db\Adapter\Adapter;

class AbstractModel
{

    /**
     * @var $adapter Adapter
     */
    protected $adapter;

    /**
     * @param Adapter $adapter
     */
    public function setAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @return Adapter
     */
    public function getAdapter()
    {
        return $this->adapter;
    }
}