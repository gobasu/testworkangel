<?php
/**
 * Created by PhpStorm.
 * User: Michał
 * Date: 2015-01-10
 * Time: 22:07
 */
namespace Application\Model;

/**
 * Uses CurrencyWebservice
 */
class CurrencyConverter extends AbstractModel
{
    const ERR_EMPTY_TRANSACTIONS = 'Transactions array cannot be empty';

    /**
     * @var CurrencyWebService
     */
    protected $currencyWebService;

    /**
     * Of course I could provide method convert and sum separately, but this way I use only one foreach
     * @param array $transactions
     * @return array
     */
    public function convertAndSum(array $transactions)
    {
        if (empty($transactions)) {
            throw new \UnexpectedValueException(self::ERR_EMPTY_TRANSACTIONS);
        }
        //Array where I keep sum of day income in '£'
        $Sum = array();
        foreach ($transactions as $key => $transaction) {
            //Split value to array - I will get currency symbol and income divided
            $transaction['value'] = $this->splitAndCheck($transaction['value']);
            //If '£' nothing to do
            if ($transaction['value']['currency'] != '£') {
                $exchangeRate = $this->currencyWebService->getExchangeRate($transaction['value']['currency']);
                $transaction['value']['amount'] = $exchangeRate * $transaction['value']['amount'];
                $transaction['value']['currency'] = '£';
            }
            if (empty($Sum[$transaction['date']])) {
                $Sum[$transaction['date']] = 0;
            }
            $Sum[$transaction['date']] += $transaction['value']['amount'];
            $transactions[$key] = $transaction;
        }
        return array(
            'transactions' => $transactions,
            'sum' => $Sum,
        );
    }


    /**
     * @param $cws
     */
    public function setCurrencyWebService($cws)
    {
        $this->currencyWebService = $cws;
    }

    /**
     * @return Transactions
     */
    public function getCurrencyWebService()
    {
        return $this->currencyWebService;
    }

    /**
     * !!!!IMPORTANT!!! I made this method because i don't know If I can divide data from CSV  - CURRENCY FROM AMOUNT!!!!!!
     * @param $income
     * @return array
     */
    private function splitAndCheck($income)
    {
        for ($i = 1; $i <= 3; $i++) {
            $tmp = substr($income, 0, $i);
            if ($tmp == '$' || $tmp == '€' || $tmp == '£') {
                return array(
                    'currency' => $tmp,
                    'amount' => substr($income, $i),
                );
            }
        }
    }
}