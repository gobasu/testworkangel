<?php

/**
 * Created by PhpStorm.
 * User: Michał
 * Date: 2015-01-10
 * Time: 22:07
 */
namespace Application\Model;

use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Stdlib\Hydrator\Reflection as ReflectionHydrator;

class Merchant extends AbstractModel
{
    const ERR_MERCHANT_ID_NUMERIC = 'Merchant Id must be numeric';
    const ERR_MERCHANT_ID_NOT_EXIST = "Merchant with provided ID doesn't exist";

    /**
     * @var Transactions
     */
    protected $transactionsModel;

    /**
     * Will return ResultSet containing Merchant object - packed in Merchant Entity (object factory way)
     * @param $id
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getMerchant($id)
    {
        //Check if merchant id is numeric - otwherwise throw exception
        if (!is_numeric($id)) {
            throw new \InvalidArgumentException(self::ERR_MERCHANT_ID_NUMERIC);
        }
        //Get required Merchant as pdo result object
        $pdoRes = $this->adapter->query('SELECT * FROM merchant WHERE id=' . $id . ' LIMIT 1')->execute();
        if(!$pdoRes->count()) {
            throw new \InvalidArgumentException(self::ERR_MERCHANT_ID_NOT_EXIST);
        }
        //Provide result to Hydrator
        $resultSet = new HydratingResultSet(new ReflectionHydrator, new \Application\Entity\Merchant());
        $resultSet->initialize($pdoRes);
        //Here I get current Merchant from resultSet as Merchant Entity Object
        /**
         * @var $merchant \Application\Entity\Merchant
         */
        $merchant = $resultSet->current();

        //Fill Merchant with transactions
        if($pdoRes->count()) {
            $merchant->setTransactions($this->transactionsModel->getTransaction($id)->toArray());
        }
        return $merchant;
    }

    /**
     * @param $tm
     */
    public function setTransactionsModel($tm) {
        $this->transactionsModel = $tm;
    }

    /**
     * @return Transactions
     */
    public function getTransactionsModel() {
        return $this->transactionsModel;
    }
}