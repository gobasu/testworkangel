<?php

namespace Application\Entity;

class Merchant
{
    /**
     * @var int $id
     */
    protected $id;

    /**
     * @var array $transactions
     */
    protected $transactions;

    /**
     * Set merchant id
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns merchant id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns full transaction list
     * @return array
     */
    public function getTransactions()
    {;
        return $this->transactions;
    }

    /**
     * @param array $transactions
     */
    public function setTransactions($transactions)
    {
        $this->transactions = $transactions;
    }
}