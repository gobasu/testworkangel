<?php
/**
 * Global configuration - only important confs
 */
return array(
    'service_manager' => array(
        'factories' => array(
            //Sqlite Adapter
            'db' => function ($sm) {
                return new \Zend\Db\Adapter\Adapter(array(
                    'driver' => 'Pdo_Sqlite',
                    'database' => __DIR__ . '/../../db/db'
                ));
            },
        ),
    ),
);
